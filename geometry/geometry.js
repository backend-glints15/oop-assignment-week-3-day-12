class Geometry {
    constructor(name, type) {
        // make abstract
        if (this.constructor === Geometry) {
            throw new Error('Cannot instantiate from Abstract Class');
            // because it's abstract
        }

        this._name = name;
        this._type = type;
    }

    hello() {
        console.log(`I'm Geometry`);
    }
}

module.exports = Geometry;