const ThreeDimention = require('./threeDimention');

class Cone extends ThreeDimention {
    constructor(radius, height) {
        super('Cone');
        this._radius = radius;
        this._height = height;
    }

    calculateSurfaceArea() {
        super.calculateSurfaceArea();

        let garisPelukis = ((this._radius ** 2) + (this._height ** 2)) ** 0.5;
        // console.log(garisPelukis);
        let baseArea = Math.PI * this._radius * (this._radius + garisPelukis);
        return Math.round(baseArea);
    }

    calculateVolume() {
        super.calculateVolume();

        let volume = Math.PI * this._radius ** 2 * this._height / 3;
        // console.log(volume);
        return Math.round(volume);
    }
}

module.exports = Cone;

const cone = new Cone(10, 12)
console.log(cone.calculateVolume());
