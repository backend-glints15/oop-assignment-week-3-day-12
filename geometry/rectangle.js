const TwoDimention = require('./twoDimention');

class Rectangle extends TwoDimention {
    constructor(width, length) {
        super('Rectangle');
        this._width = width;
        this._length = length;
    }

    // overridding method
    calculateArea() {
        // call parent class that closest
        super.calculateArea();

        return this._width * this._length;
    }

    // overloading
    // calculateCircumference(who) {
    // console.log(`Who you are? ${who}`);
    // }

    calculateCircumference() {
        super.calculateCircumference();
        return (this._width + this._length) * 2;
    }
}

module.exports = Rectangle;

// let rectangleOne = new Rectangle(10, 11);
// // console.log(squareOne);
// console.log(rectangleOne.calculateArea());
// console.log(rectangleOne.calculateCircumference());