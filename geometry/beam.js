const ThreeDimention = require("./threeDimention");

class Beam extends ThreeDimention {
  constructor(length, width, height) {
    super("Beam");
    this._length = length;
    this._width = width;
    this._height = height;
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();

    return 2 * (this._length * this._width) + 2 * (this._length * this._width) + 2 * (this._width * this._height);

  }

  calculateVolume() {
    super.calculateVolume();

    return this._length * this._width * this._height;

  }
}

module.exports = Beam;

