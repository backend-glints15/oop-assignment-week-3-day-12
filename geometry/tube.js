const ThreeDimention = require("./threeDimention");

class Tube extends ThreeDimention {
  // rumus tabung v = phi *r*r *t;
  // rumus luas tabung l= 2* phi * r * h + 2 * phi r * r
  constructor(rad, height) {
    super("Tube");
    this.rad = rad;
    this.height = height;
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    let phi = 3.14;
    let luas = (2 * phi * this.rad * this.height) + (2 * phi * this.rad ** 2);
    return Math.round(luas);
  }

  calculateVolume() {
    super.calculateVolume();
    let phi = 3.14;
    let volume = phi * this.rad ** 2 * this.height;
    return Math.round(volume);
  }
}
module.exports = Tube;
