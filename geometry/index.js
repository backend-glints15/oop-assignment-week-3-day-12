exports.Rectangle = require('./rectangle');
exports.Square = require('./square');
exports.Triangle = require('./triangle');
exports.SquarePyramid = require('./squarePyramid');
exports.Cone = require('./cone');
exports.Tube = require("./tube");
exports.Beam = require('./beam');
