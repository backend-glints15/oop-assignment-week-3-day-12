const Geometry = require("./geometry");

class TwoDimention extends Geometry {
    constructor(name) {
        super(name, 'Two Dimention');
        // new Geometry('Square', 'Two Dimention');

        // make abstract
        if (this.constructor === TwoDimention) {
            throw new Error('Cannot instantiate from Abstract Class');
            // because it's abstract
        }
    }

    // #calculateArea() {   // private method/function
    calculateArea() {
        console.log(`Calculate ${this._name} area!`);
    }

    // to call private method
    // callCalArea() {
    //     this.#calculateArea();
    // }

    calculateCircumference() {
        console.log(`Calculate ${this._name} Circumference!`);
    }
}

module.exports = TwoDimention;

// let one = new TwoDimention('Square');
// console.log(one);
// one.hello();