const { Rectangle, Square, Triangle, Cone, Tube, Beam } = require('./geometry');

let rectangleOne = new Rectangle(10, 11)
let squareOne = new Square(12)
let triangleOne = new Triangle(13, 14)
let coneOne = new Cone(7, 10);
let tubeOne = new Tube(7, 9);
let beamOne = new Beam(6, 6, 6);

/* calculate area 2D */
console.log('\n--- Area 2D ---');
let a = rectangleOne.calculateArea();
let b = squareOne.calculateArea();
let c = triangleOne.calculateArea();
let d = a + b + c;
console.log(d);

/* calculate circumference 2D */
console.log('\n--- Circumference 2D ---');
let e = rectangleOne.calculateCircumference();
let f = squareOne.calculateCircumference();
let g = triangleOne.calculateCircumference();
let h = e + f + g;
console.log(h);

console.log('\n\n----- Tugas -----\n');
/* calculate surface area 3D */
console.log('\n--- Surface Area 3D ---');
let coneSurfaceArea = coneOne.calculateSurfaceArea();
let tubeSurfaceArea = tubeOne.calculateSurfaceArea();
let beamSurfaceArea = beamOne.calculateSurfaceArea();
let totalSurfaceArea = coneSurfaceArea + tubeSurfaceArea + beamSurfaceArea;
console.log(totalSurfaceArea);

/* calculate volume 3D */
console.log('\n--- Volume 3D ---');
let coneVolume = coneOne.calculateVolume();
let tubeVolume = tubeOne.calculateVolume();
let beamVolume = beamOne.calculateVolume();
let totalVolume = coneVolume + tubeVolume + beamVolume;
console.log(totalVolume);